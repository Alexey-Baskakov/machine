/**
 * Класс учета нагрузки, как контекста
 */
public class Blank {
    private Activity state;

    /**
     * Метод для смены состояний
     * @param state - новое состояние
     */
    public void setState(Activity state) {
        this.state = state;
    }

    /**
     * Метод с логикой автомата
     * @param day - день месяца
     */
    public void action(int day) {
        state.action(this, day, Activity.random.nextInt(3) <= 1);
    }
}
