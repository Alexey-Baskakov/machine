import java.util.Random;

public interface Activity {
    Random random = new Random();
    /**
     * Метод с функцией автомата
     * @param blank - класс контекста
     * @param day - день месяца
     * @param isNext - рандомное значение, имитирует работу автомата
     */
    void action(Blank blank, int day, boolean isNext);
}

/**
 * Состояние 1 из 6, напоминание
 * В течении месяца напоминает преподователю о необходимости заполнить лист учета нагрузки
 * Если преподователь начал заполнять, прекращает работу
 */
class ReminderAccountForLoad implements Activity {
    public void action(Blank blank, int day, boolean isNext) {
        if (day == 1 || !isNext) {
            System.out.println("Необходимо заполнить лист учета нагрузки");
        } else {
            System.out.println("Преподователь увидел напоминание...");
            blank.setState(new ReceptionLUN());
        }
    }
}

/**
 * Состояние 2 из 6, преподователь
 * В течении месяца (до 27 числа, заполняет лист учета нагрузки)
 * После 27 числа отправляет его на проверку секретарю кафедры
 */
class ReceptionLUN implements Activity {
    public void action(Blank blank, int day, boolean isNext) {
        if (day <= 21) {
            System.out.println("Преподователь заполняет лист учета нагрузки, день: " + day);
        } else {
            System.out.println("Преподователь заполнил лист учета нагрузки, посылает его секретарю кафедры");
            blank.setState(new ReceptionLUNSecretaryOfDepartment());
        }
    }
}

/**
 * Состояние 3 из 6, секретарь кафедры
 * Условия: в день обращения утвердить или не утвердить лист учета нагрузки
 * Если да, передать начальнику учебно-методического отедал
 * Если нет, вернуть преподователю
 */
class ReceptionLUNSecretaryOfDepartment implements Activity {
    public void action(Blank blank, int day, boolean isNext) {
        if (isNext) {
            System.out.println("Лист утвержден секретарем кафедры, секретарь посылает лист начальнику учебно-методического отдела...");
            blank.setState(new ReceptionNUMO());
        } else {
            System.out.println("Лист не утвержден, посылаю его обратно преподователю...");
            blank.setState(new ReceptionLUN());
        }
    }
}

/**
 * Состояние 4 из 6, начальник учебно-методического отдела
 * Условия: утвердить или не утвердить лист учета нагрузки в течении недели
 * Если утвердил, передать заведующему кафедры
 * Если нет, вернуть преподователю
 */
class ReceptionNUMO implements Activity {
    private int week = 1;

    public void action(Blank blank, int day, boolean isNext) {
        if (!isNext && week <= 7) {
            System.out.println("Идет проверка листа учета нагрузки начальником учебно-методического отдела, день : " + week + " из 7");
            week++;
        } else if (Activity.random.nextInt(3) <= 1) {
            System.out.println("Начальником учебно-методического отдела утвердил лист и передал его заведующему кафедрой");
            blank.setState(new ReceptionOfHeadOfDepartment());
        } else {
            System.out.println("Начальником учебно-методического отдела не утвердил лист и передал его обратно преподователю");
            blank.setState(new ReceptionLUN());
        }
    }
}

/**
 * Состояние 5 из 6, заведующий кафедры
 * Условия: утвердить или не утвердить лист учета нагрузки в течении 3 дней
 * Если не утвердил, вернуть преподователю
 * Если утвердил передать бухгалтеру
 */
class ReceptionOfHeadOfDepartment implements Activity {
    private int threeDay = 1;

    public void action(Blank blank, int day, boolean isNext) {
        if (!isNext && threeDay <= 3) {
            System.out.println("Заведующий кафедрой утверждает расчетный лист, день: " + threeDay + " из 3");
            threeDay++;
        } else if (Activity.random.nextInt(3) <= 1) {
            System.out.println("Заведующий кафедры утвердил лист и передал его Бухгалтеру");
            blank.setState(new ReceptionOfAccountant());
        } else {
            System.out.println("Заведующий кафедры не утвердил лист и предал его обратно преподователю");
            blank.setState(new ReceptionLUN());
        }
    }
}

/**
 * Состояние 6 из 6, бухгалтер
 * Условия: утвердить или не утвердить лист учета нагрузки в течении 3 дней
 * Если нет, вернуть его преподавателю
 */
class ReceptionOfAccountant implements Activity {
    private int threeDay = 1;

    public void action(Blank blank, int day, boolean isNext) {
        if (!isNext && threeDay <= 3) {
            System.out.println("Бухгалтер утверждает расчетный лист, день: " + threeDay + " из 3");
            threeDay++;
        } else if (Activity.random.nextInt(3) <= 1) {
            System.out.println("Бухгалтер утвердил лист..., автомат закончился...");
            throw new NullPointerException("Цикл закончился");
        } else {
            System.out.println("Бухгалтер не утвердил лист и предал его обратно преподователю");
            blank.setState(new ReceptionLUN());
        }
    }
}


